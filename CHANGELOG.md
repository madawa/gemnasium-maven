# Gemnasium Maven analyzer changelog

## v2.2.4
- Run `mvn install` before analysis to fix multi-modules support when using internal dependencies between modules

## v2.2.3
- Fix `DS_EXCLUDED_PATHS` not applied to dependency files (!13)

## v2.2.2
- Fix dependency list, include dependency files which do not have any vulnerabilities (!11)

## v2.2.1
- Sort the dependency files and their dependencies (!9)

## v2.2.0
- List the dependency files and their dependencies (!8)

## v2.1.2
- Bump common to v2.1.6
- Bump gemnasium to v2.1.2

## v2.1.1
- Fix multi-modules support

## v2.1.0
- Bump common to v2.1.4, introduce remediations
- Bump gemnasium to v2.1.1, introduce stable report order

## v2.0.0
- Switch to new report syntax with `version` field

## v1.1.0
- Add dependency (package name and version) to report
- Improve vulnerability name, message and compare key

## v1.0.0
- Initial release
