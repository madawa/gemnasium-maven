package plugin

import (
	"os"

	"gitlab.com/gitlab-org/security-products/analyzers/common/v2/plugin"
)

func Match(path string, info os.FileInfo) (bool, error) {
	if info.Name() == "pom.xml" {
		return true, nil
	}
	return false, nil
}

func init() {
	plugin.Register("gemnasium-maven", Match)
}
